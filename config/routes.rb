Rails.application.routes.draw do
  root to: "inicio#index"
  get '/inicio', to: "inicio#index"
  get '/galeria', to:  "galeria#index"
  get '/servicos', to: "servicos#index"
  get '/sobre', to: "sobre#index"
  get '/construcao', to: "construcao#index"
  get '/contato', to: "contato#index"
end
